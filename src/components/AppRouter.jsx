import React, {Component} from 'react';
import {BrowserRouter, Route} from 'react-router-dom';
import HomePage from './HomePage';
import AppAbout from './AppAbout';
import AppContact from './AppContact';

class AppRouter extends Component {
    render () {
        return(
            <div>
                <BrowserRouter>
                <div>
                    <Route exact path='/' component={HomePage} />
                    <Route path='/contact' component={AppContact} />
                    <Route path='/about' component={AppAbout} />
                </div>
                </BrowserRouter>
            </div>
        );
    }
}

export default AppRouter