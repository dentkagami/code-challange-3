import React, {Component} from 'react';
import AppHeader from './AppHeader';

class HomePage extends Component{
    render() {
        return (
            <div>
                <AppHeader />
            </div>
        );
    }
}

export default HomePage